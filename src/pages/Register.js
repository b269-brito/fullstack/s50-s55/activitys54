import {useState, useEffect, useContext} from 'react'
import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register() {

    const {user, setUser} = useContext(UserContext);

    // to store and manage value of the input fields
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);



    useEffect(() => {
        if(( email !== "" && password1 !== "" && password2 !=="" && firstName !== "" && lastName !== "" && mobileNumber !== "" && mobileNumber.length > 10) && password1 === password2) {
            setIsActive(true);
        } else {
            setIsActive(false);
        };
    }, [email, password1, password2, firstName, lastName, mobileNumber]);

    // function to simulate user registration
    function registerUser(e) {
        e.preventDefault();

         fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                
            })
        })
        .then(res => res.json())
        .then(data => {
       
            console.log(data);

            if(typeof data.access !== "undefined") {
                

                Swal.fire({
                    title: " Registration Successful!",
                    icon: "success",
                    text: "Welcome to Zuitt!"

                })
            } else {
                Swal.fire({
                    title: "Email Already exists!",
                    icon: "error",
                    text: "Please check your login details and try again!"

                })
            };
    });

        // Clear input fields
        setEmail("");
        setPassword1("");
        setPassword2("");
        setFirstName("");
        setLastName("");
        setMobileNumber("");



    };

    return (
        (user.id !== null) ?
        <Navigate to= "/courses"/>
        :

        <Form onSubmit={(e) => registerUser(e)} >

          <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="Text" 
                    placeholder="First Name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

              <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Last Name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>


            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

              <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="mobileNumber" 
                    placeholder="+63 000 000 0000" 
                    value={mobileNumber}

                    onChange={e => setMobileNumber(e.target.value)}
                    required
                />
            </Form.Group>


            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn" onClick={() => Swal.fire({
                    title: " Registration Successful!",
                    icon: "success",
                    text: "Welcome to Zuitt!"

                })} >
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }
            
        </Form>
    )

}

